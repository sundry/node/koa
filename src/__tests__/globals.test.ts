import { server } from '..';

const startServer = new Promise(resolve => server.on('listening', resolve));

before(async () => {
  await new Promise(resolve => resolve(startServer));
});
after(async () => {
  await server.close();
});
