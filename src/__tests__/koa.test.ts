import { expect } from 'chai';
import axios from './axios';

describe('koa routes', () => {
  it('should GET 404 /', async () => {
    const options: any = {
      validateStatus: false,
    };
    const res = await axios('/', options);
    expect(res.status).to.equal(404);
  });
});
