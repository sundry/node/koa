import { expect } from 'chai';
import axios from '../../../../../__tests__/axios';
import RedisClient from '../client';

const key = 'test';
const value = {
  id: 1,
  name: 'Andrew',
  likes: ['bowling', 'basketball', 'bbq'],
  dislikes: ['unfiltered banana water'],
};

describe('koa routes', () => {
  describe('cache', () => {
    before(async () => {
      await RedisClient.flushAllAsync();
    });

    it('should get a blank object', async () => {
      const res = await axios(`/api/v1/cache/${key}`);
      expect(res.status).to.equal(200);
      expect(res.data).to.deep.equal({});
    });

    it('should set a blank object', async () => {
      const data = {};
      const res = await axios.post(`/api/v1/cache/${key}`, data);
      expect(res.status).to.equal(200);
      expect(res.data).to.deep.equal(data);
    });

    it('should set the value object', async () => {
      const res = await axios.post(`/api/v1/cache/${key}`, value);
      expect(res.status).to.equal(200);
      expect(res.data).to.deep.equal(value);
    });

    it('should get the value object', async () => {
      const res = await axios(`/api/v1/cache/${key}`);
      expect(res.status).to.equal(200);
      expect(res.data).to.deep.equal(value);
    });
  });
});
