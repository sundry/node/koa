import { BaseContext } from 'koa';
import Router from 'koa-router';
import RedisClient from './client';

const router = new Router();

router.get('/:key', async (ctx: BaseContext) => {
  const { key } = ctx.params;
  const value = await RedisClient.getAsync(key);
  ctx.body = JSON.parse(value) || {};
  ctx.status = 200;
});

router.post('/:key', async (ctx: BaseContext) => {
  const { key } = ctx.params;
  const value = ctx.request.body;
  await RedisClient.setAsync(key, JSON.stringify(value));
  ctx.body = value;
  ctx.status = 200;
});

export default router;
