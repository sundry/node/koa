import redis from 'redis';
import { promisify } from 'util';

const client = redis.createClient();

export default {
  ...client,
  flushAllAsync: promisify(client.flushall).bind(client),
  getAsync: promisify(client.get).bind(client),
  setAsync: promisify(client.set).bind(client),
};
