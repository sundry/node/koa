import Router from 'koa-router';
import HealthCheck from './healthcheck';
import Info from './info';
import Cache from './cache';
import Redis from './redis';

const router = new Router();

router.use('/healthcheck', HealthCheck.routes(), HealthCheck.allowedMethods());
router.use('/info', Info.routes(), Info.allowedMethods());
router.use('/cache', Cache.routes(), Cache.allowedMethods());
router.use('/redis', Redis.routes(), Redis.allowedMethods());

export default router;
