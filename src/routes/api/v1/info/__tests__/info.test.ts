import { expect } from 'chai';
import axios from '../../../../../__tests__/axios';

describe('koa routes', () => {
  it('should get info', async () => {
    const res = await axios('/api/v1/info');
    expect(res.status).to.equal(200);
  });
});
