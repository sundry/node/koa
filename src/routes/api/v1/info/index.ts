import Router from 'koa-router';
const router = new Router();

router.get('/', async ctx => {
  ctx.body = {
    env: process.env.NODE_ENV,
    hostname: require('os').hostname(),
    tag: process.env.DOCKER_TAG,
  };
});

export default router;
