import Router from 'koa-router';

const router = new Router();

router.get('/', (ctx, next) => {
  ctx.status = 200;
});

router.get('/err', (ctx, next) => {
  throw new Error('Not OK');
});

export default router;
