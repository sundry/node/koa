import { expect } from 'chai';
import axios from '../../../../../__tests__/axios';

describe('koa routes', () => {
  it('should get healthcheck', async () => {
    const res = await axios('/api/v1/healthcheck');
    expect(res.status).to.equal(200);
  });
  it('should test error', async () => {
    const options: any = {
      validateStatus: false,
    };
    const res = await axios('/api/v1/healthcheck/err', options);
    expect(res.status).to.equal(500);
  });
});
