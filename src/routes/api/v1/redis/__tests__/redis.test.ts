import { expect } from 'chai';
import axios from '../../../../../__tests__/axios';
import RedisClient from '../client';

describe('koa routes', () => {
  describe('redis', () => {
    before(async () => {
      await RedisClient.flushallAsync();
    });

    it('should set my-counter', async () => {
      const res = await axios(`/api/v1/redis/set/my-counter/0`);
      expect(res.status).to.equal(200);
      expect(res.data).to.equal('OK');
    });

    it('should increment my-counter', async () => {
      for await (let i of [1, 2, 3, 4, 5]) {
        const res = await axios(`/api/v1/redis/incr/my-counter`);
        expect(res.status).to.equal(200);
        expect(res.data).to.equal(i);
      }
    });

    it('should get my-counter', async () => {
      const res = await axios(`/api/v1/redis/get/my-counter`);
      expect(res.status).to.equal(200);
      expect(res.data).to.equal(5);
    });

    it('should lpush my-list', async () => {
      const res1 = await axios(`/api/v1/redis/lpush/my-list/a`);
      expect(res1.status).to.equal(200);
      const res2 = await axios(`/api/v1/redis/lpush/my-list/b`);
      expect(res2.status).to.equal(200);
    });

    it('should rpush my-list', async () => {
      const res1 = await axios(`/api/v1/redis/rpush/my-list/c`);
      expect(res1.status).to.equal(200);
      const res2 = await axios(`/api/v1/redis/rpush/my-list/d`);
      expect(res2.status).to.equal(200);
    });

    it('should get my-list', async () => {
      const res = await axios(`/api/v1/redis/lrange/my-list/0/-1`);
      expect(res.status).to.equal(200);
      expect(res.data).to.deep.equal(['b', 'a', 'c', 'd']);
    });

    it('should set my-set', async () => {
      const set = [1, 1, 2, 3, 3, 4, 4, 4, 4, 5];
      const output = [];
      for await (let i of set) {
        const res1 = await axios(`/api/v1/redis/sadd/my-set/${i}`);
        expect(res1.status).to.equal(200);
        const res2 = await axios(`/api/v1/redis/smembers/my-set`);
        expect(res2.status).to.equal(200);
        if (res1.data === 1) {
          output.push(i.toString());
          expect(res2.data).to.deep.equal(output);
        }
      }
    });

    it('should get my-set', async () => {
      const res = await axios(`/api/v1/redis/smembers/my-set`);
      expect(res.status).to.equal(200);
      expect(res.data).to.deep.equal(['1', '2', '3', '4', '5']);
    });
  });
});
