import { BaseContext } from 'koa';
import Router from 'koa-router';
import RedisClient from './client';

const router = new Router();

router.get('/:func/:key/:value+', async (ctx: BaseContext) => {
  const { func, key, value } = ctx.params;
  const result = await RedisClient[`${func}Async`].apply(RedisClient, [
    key,
    ...value.split('/'),
  ]);
  ctx.body = result;
  ctx.status = 200;
});

router.get('/:func/:key', async (ctx: BaseContext) => {
  const { func, key } = ctx.params;
  const result = await RedisClient[`${func}Async`](key);
  ctx.body = result;
  ctx.status = 200;
});

router.get('/:func', async (ctx: BaseContext) => {
  const { func } = ctx.params;
  const result = await RedisClient[`${func}Async`]();
  ctx.body = result;
  ctx.status = 200;
});

export default router;
