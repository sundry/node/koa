import Router from 'koa-router';
import ApiRoutes from './api';

const router = new Router();

router.use('/api', ApiRoutes.routes(), ApiRoutes.allowedMethods());

export default router;
